import { Client } from 'minio'

export module s3 {
  const client = new Client({
    endPoint: 's3.genshinjikken.fun',
    port: 443,
    useSSL: true,
    accessKey: process.env.MINIO_USER!,
    secretKey: process.env.MINIO_PASS!,
  })

  export const S3_ENDPOINT = process.env.S3_ENDPOINT
  export const S3_BUCKET = process.env.S3_BUCKET

  export const putObject = (name: string, base64: string) => {
    return new Promise<{ etag: string, url: string }>((resolve, reject) => {
      const buffer = Buffer.from(base64.replace('data:image/webp;base64,', ''), 'base64')
      const metadata = {
        'Content-Type': 'image/webp'
      }
      client.putObject(`genshin`, name, buffer, buffer.length, metadata, (err, etag) => {
        err ? reject(err) : resolve({
          etag: etag.etag,
          url: `${s3.S3_ENDPOINT}/${s3.S3_BUCKET}/${name}`
        })
      })
    })
  }
}

export default s3