import { ArtifactPair, ArtifactQuadruplet, Build, Character, Weapon } from "@prisma/client";

export type CharacterWithBuilds = Character & {
  build: BuildWithArtifactsAndWeapon
}

export type BuildWithArtifactsAndWeapon = Omit<Build, 'id' | 'characterId'>