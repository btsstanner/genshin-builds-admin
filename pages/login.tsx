import { useState } from 'react'
import Router from 'next/router'
import { Button, Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
import useUser from '../hooks/useUser'

const LoginForm = () => {
  const { mutate } = useUser({
    redirectTo: '/',
  })

  const [body, setBody] = useState({
    username: '',
    password: '',
  })

  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='black' textAlign='center'>
          <Image src='/primogem.webp' /> Log-in
        </Header>
        <Form size='large'>
          <Segment>
            <Form.Input
              fluid icon='user'
              iconPosition='left'
              placeholder='Username'
              onChange={(e) => setBody(b => { return { ...b, username: e.target.value } })}
            />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              type='password'
              onChange={(e) => setBody(b => { return { ...b, password: e.target.value } })}
            />
            <Button color='black' fluid size='large' onClick={
              async function handleSubmit(event) {
                event.preventDefault()

                try {
                  const res = await fetch('/api/login', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(body),
                  })
                  const json = await res.json()
                  mutate(json)
                  Router.push('/')
                } catch (error) {
                  console.error('cazzi')
                }
              }}>
              Login
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  )
}

export default LoginForm