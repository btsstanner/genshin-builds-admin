import { Weapon } from '@prisma/client'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { omit } from 'radash'
import { Fragment, useEffect, useState } from 'react'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import useSWR, { Fetcher, useSWRConfig } from 'swr'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { useUpload } from '../../../hooks/useUpload'
import { allKeysSet } from '../../../utils/objects'
import { pascalCase } from '../../../utils/strings'

const Home: NextPage = () => {
  const [weapon, setWeapon] = useState<Omit<Weapon, 'buildId' | 'createdAt'>>({
    id: '',
    name: '',
    type: 'BOW',
    img: '',
    baseATK: [0, 0],
    is5Star: false,
    passiveEffect: '',
    subStatType: '',
    subStatVal: []
  })

  const [uploaded, setUploaded] = useState(false)
  const [uploading, setUploading] = useState(false)

  const fetcher: Fetcher<Weapon, string> = (...args) => fetch(...args).then(res => res.json())
  const router = useRouter()
  const { id } = router.query

  const { data } = useSWR(`/api/weapons/${id}`, fetcher)
  const { mutate } = useSWRConfig()

  useEffect(() => {
    if (data) {
      setWeapon(data)
      setOldFile(data.img ?? '')
    }
  }, [data])

  const editWeapon = () => {
    setUploading(true)
    fetch(`/api/weapons/${id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(omit(weapon, ['id']))
    })
      .then(() => setUploaded(true))
      .then(() => setUploading(false))
      .then(() => mutate('/api/weapons'))
  }

  const [file, setFile] = useState<{ preview: string, name: string }>()
  const [oldFile, setOldFile] = useState("")

  const setFileCallback = (file: any) => setFile(file)
  const setDataCallback = (result: string) => setWeapon(w => { return { ...w, img: result } })

  const getRootProps = useUpload(setFileCallback, setDataCallback)

  return (
    <Layout>
      <Header as='h2' dividing>
        Edit weapon
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploaded ? <Loader dismissable success title='Success' text="Uploaded weapon's data" /> : null}
          <Form loading={!data || uploading}>
            <Form.Group widths='equal'>
              <Form.Input
                defaultValue={data?.name}
                fluid
                label='Weapon name'
                placeholder='Acqua Simulacra'
                loading={!data}
                onChange={e => setWeapon((w) => { return { ...w, name: pascalCase(e.target.value) } })}
              />
              <Form.Select
                label='Type'
                value={data?.type}
                options={
                  [
                    { key: 'claymore', text: 'Claymore', value: 'CLAYMORE' },
                    { key: 'polearm', text: 'Polearm', value: 'POLEARM' },
                    { key: 'sword', text: 'Sword', value: 'SWORD' },
                    { key: 'bow', text: 'Bow', value: 'BOW' },
                    { key: 'catalyst', text: 'Catalyst', value: 'CATALYST' },
                  ]
                }
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                defaultValue={data?.baseATK[0]}
                label='Base ATK lv.1'
                placeholder='100'
                error={isNaN(weapon.baseATK[0])}
                loading={!data}
                onChange={e => setWeapon((w) => {
                  const [_, lv90] = w.baseATK
                  return { ...w, baseATK: [Number(e.target.value), lv90] }
                })}
              />
              <Form.Input
                fluid
                defaultValue={data?.baseATK[1]}
                label='Base ATK lv.90'
                placeholder='600'
                loading={!data}
                error={isNaN(weapon.baseATK[1])}
                onChange={e => setWeapon((w) => {
                  const [lv1, _] = w.baseATK
                  return { ...w, baseATK: [lv1, Number(e.target.value)] }
                })}
              />
              <Form.Input
                fluid
                defaultValue={data?.subStatType}
                label='Sub stat type'
                placeholder='Crit DMG%'
                loading={!data}
                onChange={e => setWeapon((w) => { return { ...w, subStatType: e.target.value } })}
              />
              <Form.Input
                fluid
                defaultValue={data?.subStatVal[0]}
                label='Sub stat lv.1'
                placeholder='10.0'
                loading={!data}
                onChange={e => setWeapon((w) => {
                  const [_, lv90] = w.subStatVal
                  return { ...w, subStatVal: [e.target.value, lv90] }
                })}
              />
              <Form.Input
                fluid
                defaultValue={data?.subStatVal[1]}
                label='Sub stat lv.90'
                placeholder='68.2'
                loading={!data}
                onChange={e => setWeapon((w) => {
                  const [lv1, _] = w.subStatVal
                  return { ...w, subStatVal: [lv1, e.target.value] }
                })}
              />
            </Form.Group>
            <Form.Group inline>
              <label>5 star</label>
              <Form.Radio
                label=''
                defaultChecked={data?.is5Star}
                checked={weapon.is5Star}
                onClick={() => setWeapon((w) => { return { ...w, is5Star: !w.is5Star } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                rows={6}
                defaultValue={data?.passiveEffect ?? ''}
                label='Passive effect'
                placeholder='You can write in Markdown here!'
                onChange={e => setWeapon((w) => { return { ...w, passiveEffect: e.target.value } })}
              />
            </Form.Group>
            <Grid columns={5}>
              <Grid.Column>
                <label>Image</label>
                <Image src={oldFile} fluid></Image>
              </Grid.Column>
              <Grid.Column>
                {
                  file?.preview ?
                    <Fragment>
                      <label>Will be replaced with</label>
                      <Image src={weapon.img ?? data?.img} fluid></Image>
                    </Fragment> :
                    null
                }
              </Grid.Column>
            </Grid>
            <Grid columns={6}>
              <Grid.Column>
                <div {...getRootProps()}>
                  <Form.Button label='Image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
            </Grid>
            <Form.Button
              style={{ marginTop: '2rem' }}
              onClick={() => editWeapon()}
              disabled={!allKeysSet<Weapon>(weapon)}
            >
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
