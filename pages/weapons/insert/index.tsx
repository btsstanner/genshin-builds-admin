import { Weapon, WeaponType } from '@prisma/client'
import type { NextPage } from 'next'
import { useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { allKeysSet } from '../../../utils/objects'
import { pascalCase } from '../../../utils/strings'

const Home: NextPage = () => {
  const [weapon, setWeapon] = useState<Omit<Weapon, 'id' | 'buildId' | 'createdAt'>>({
    name: '',
    img: '',
    baseATK: [0, 0],
    is5Star: false,
    passiveEffect: '',
    subStatType: '',
    subStatVal: [],
    type: 'BOW',
  })

  const [uploading, setUploading] = useState(false)
  const [doneUploading, setDoneUploading] = useState(false)

  const addWeapon = () => {
    setUploading(true)
    fetch(`/api/weapons`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(weapon)
    })
      .then(() => setUploading(false))
      .then(() => setDoneUploading(true))
  }

  const [file, setFile] = useState<{ preview: string, name: string }>()

  const { getRootProps, } = useDropzone({
    accept: { 'image/*': [] },
    onDrop: acceptedFiles => {
      const file = Object.assign(acceptedFiles[0], {
        preview: URL.createObjectURL(acceptedFiles[0])
      })
      const reader = new FileReader()
      reader.readAsDataURL(file);
      reader.onload = (() => {
        setFile(file)
        setWeapon(w => { return { ...w, img: reader.result as string } })
      })
    }
  });

  return (
    <Layout>
      <Header as='h2' dividing>
        Add weapon
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploading ? <Loader title='Uploading' text="Uploading weapon's data" /> : null}
          {doneUploading ? <Loader dismissable success title='Success' text="Uploaded weapon's data" /> : null}
          <Form loading={uploading}>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                label='Weapon name'
                placeholder='Acqua Simulacra'
                onChange={e => setWeapon((w) => { return { ...w, name: pascalCase(e.target.value) } })}
              />
              <Form.Select
                fluid
                options={
                  [
                    { key: 'bow', text: 'Bow', value: 'BOW' },
                    { key: 'sword', text: 'Sword', value: 'SWORD' },
                    { key: 'polearm', text: 'Polearm', value: 'POLEARM' },
                    { key: 'catalyst', text: 'Catalyst', value: 'CATALYST' },
                    { key: 'claymore', text: 'Claymore', value: 'CLAYMORE' },
                  ]
                }
                label='Weapon type'
                placeholder='Select type'
                onChange={((_, { value }) => setWeapon((w) => { return { ...w, type: value! as WeaponType } }))}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                label='Base ATK lv.1'
                placeholder='100'
                error={isNaN(weapon.baseATK[0])}
                onChange={e => setWeapon((w) => {
                  const [_, lv90] = w.baseATK
                  return { ...w, baseATK: [Number(e.target.value), lv90] }
                })}
              />
              <Form.Input
                fluid
                label='Base ATK lv.90'
                placeholder='600'
                error={isNaN(weapon.baseATK[1])}
                onChange={e => setWeapon((w) => {
                  const [lv1, _] = w.baseATK
                  return { ...w, baseATK: [lv1, Number(e.target.value)] }
                })}
              />
              <Form.Input
                fluid
                label='Sub stat type'
                placeholder='Crit DMG%'
                onChange={e => setWeapon((w) => { return { ...w, subStatType: e.target.value } })}
              />
              <Form.Input
                fluid
                label='Sub stat lv.1'
                placeholder='10.0'
                onChange={e => setWeapon((w) => {
                  const [_, lv90] = w.subStatVal
                  return { ...w, subStatVal: [e.target.value, lv90] }
                })}
              />
              <Form.Input
                fluid
                label='Sub stat lv.90'
                placeholder='68.2'
                onChange={e => setWeapon((w) => {
                  const [lv1, _] = w.subStatVal
                  return { ...w, subStatVal: [lv1, e.target.value] }
                })}
              />
            </Form.Group>
            <Form.Group inline>
              <label>5 star</label>
              <Form.Radio
                label=''
                checked={weapon.is5Star}
                onClick={() => setWeapon((w) => { return { ...w, is5Star: !w.is5Star } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                label='Passive effect'
                placeholder='You can write in Markdown here!'
                rows={6}
                onChange={e => setWeapon((w) => { return { ...w, passiveEffect: e.target.value } })}
              />
            </Form.Group>
            {/* <Form.Input
              fluid
              label='Image URL'
              placeholder='https://...'
              error={!isURL(weapon.img ?? '')}
              onChange={e => setWeapon((w) => { return { ...w, img: e.target.value } })}
            /> */}
            <Grid columns={6}>
              <Grid.Column>
                <div {...getRootProps()}>
                  <Form.Button label='Image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={file?.preview} size='tiny'></Image>
              </Grid.Column>
            </Grid>
            <Form.Button
              style={{ marginTop: '2rem' }}
              onClick={() => addWeapon()/*.then(() => window.location.reload())*/}
              disabled={
                !(allKeysSet<Weapon>(weapon))
              }
            >
              <Icon name='save'></Icon>
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
