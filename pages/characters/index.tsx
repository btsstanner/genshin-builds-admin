import { Character } from '@prisma/client'
import type { NextPage } from 'next'
import Link from 'next/link'
import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { Button, Grid, Header, Icon, Image, Input, Modal, Table } from 'semantic-ui-react'
import useSWR, { Fetcher, useSWRConfig } from 'swr'
import Layout from '../../components/Layout'
import Loader from '../../components/Loader'
import useUser from '../../hooks/useUser'

const Home: NextPage = () => {
  const [openModal, setOpenModal] = useState(false)
  const [target, setTarget] = useState<Character>()

  const fetcher: Fetcher<Character[], string> = (...args) => fetch(...args).then(res => res.json())
  const { data } = useSWR('/api/characters', fetcher)

  const [workingData, setWorkingData] = useState<Character[]>(new Array<Character>())

  useEffect(() => {
    setWorkingData(data ?? [])
  }, [data])

  const search = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setWorkingData(
      (data ?? []).filter(el =>
        el.name.toLowerCase().includes(e.target.value.toLowerCase()))
    )
  }, [data])

  const { mutate } = useSWRConfig()

  const deleteWeapon = (id: string) => fetch(`/api/characters/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(() => mutate('/api/characters'))

  return (
    <Layout>
      <Header as='h2' dividing>
        Characters
      </Header>

      <Grid columns='equal'>
        <Grid.Column >
          <Link href='/characters/insert'>
            <Button>
              Add new character
            </Button>
          </Link>
        </Grid.Column>
        <Grid.Column width={12}>
          <Input icon='search' placeholder='Search...' onChange={search} fluid />
        </Grid.Column>
      </Grid>

      <Grid columns={1} style={{ marginTop: '0.25rem' }}>
        <Grid.Column>
          {!data && workingData.length === 0 ? <Loader title='Loading' text="Loading weapons' data..." /> : null}
          <Table basic='very' celled >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Weapon</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {workingData?.map(w => (
                <Table.Row key={w.id}>
                  <Table.Cell>
                    <Header as='h4' image>
                      <Image src={w.portrait ?? ''} />
                      <Header.Content>
                        {w.name}
                        <Header.Subheader>
                          Last update: {new Date(w.createdAt).toLocaleString()}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Button.Group>
                      <Link href={`/characters/update/${w.id}`}>
                        <Button>Edit</Button>
                      </Link>
                      <Button.Or text='or' />
                      <Button color='red' onClick={() => {
                        setOpenModal(true)
                        setTarget(w)
                      }}>
                        Delete
                      </Button>
                    </Button.Group>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid>
      <Modal
        basic
        open={openModal}
        size='small'
      >
        <Header icon>
          <Icon name='archive' />
          {`Delete ${target?.name}???`}
        </Header>
        <Modal.Content>
          <p>This operation is irreversible.</p>
          <p>Are you really sure to delete it?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button basic color='red' inverted onClick={() => {
            setOpenModal(false)
          }}>
            <Icon name='remove' /> No
          </Button>
          <Button color='green' inverted onClick={() => {
            deleteWeapon(target?.id ?? '')
            setOpenModal(false)
          }}>
            <Icon name='checkmark' /> Yes
          </Button>
        </Modal.Actions>
      </Modal>
    </Layout>
  )
}

export default Home
