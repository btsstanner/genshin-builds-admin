import Editor from '@monaco-editor/react'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { omit, zip } from 'radash'
import { useEffect, useState } from 'react'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import useSWR, { Fetcher } from 'swr'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { useUpload } from '../../../hooks/useUpload'
import useUser from '../../../hooks/useUser'
import { CharacterWithBuilds } from '../../../types'
import { pascalCase } from '../../../utils/strings'

const elements = ["pyro", "hydro", "anemo", "electro", "dendro", "cryo", "geo"]
const elementsOptions = elements.map(e => {
  return {
    key: e,
    text: pascalCase(e),
    value: e.toUpperCase(),
  }
})

const Home: NextPage = () => {
  const [character, setCharacter] = useState<Omit<CharacterWithBuilds, 'id' | 'buildId' | 'createdAt' | 'build' | 'teamCompId'>>({
    name: '',
    bio: '',
    notes: '',
    element: 'ANEMO',
    is5Star: false,
    portrait: '',
    splash: '',
    published: false,
  })

  const router = useRouter()
  const { id } = router.query

  const fetcher: Fetcher<CharacterWithBuilds, string> = (...args) => fetch(...args).then(res => res.json())

  const { data } = useSWR(`/api/characters/${id}`, fetcher)
  useEffect(() => {
    if (data) setCharacter(c => zip(c, omit(data, ['id'])))
  }, [data])

  const saveCharacter = () => fetch(`/api/characters/${data?.id}`, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(character)
  }).then(() => setUploaded(true))

  const [filePortrait, setPortraitFile] = useState<{ preview: string, name: string }>()
  const [fileSplash, setSplashFile] = useState<{ preview: string, name: string }>()


  const setSplashFileCallback = (file: any) => setSplashFile(file)
  const setSplashDataCallback = (data: string) => setCharacter(a => { return { ...a, splash: data } })

  const getSplashRootProps = useUpload(setSplashFileCallback, setSplashDataCallback)

  const setPortraitFileCallback = (file: any) => setPortraitFile(file)
  const setPortraitDataCallback = (data: string) => setCharacter(a => { return { ...a, portrait: data } })

  const getPortraitRootProps = useUpload(setPortraitFileCallback, setPortraitDataCallback)

  const [uploaded, setUploaded] = useState<boolean>(false)

  return (
    <Layout>
      <Header as='h2' dividing>
        Update character
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploaded ? <Loader dismissable success title='Success' text="Uploaded characters's data" /> : null}
          <Form loading={!data}>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                defaultValue={data?.name}
                label='Name'
                placeholder='Raiden Shogun'
                loading={!data}
                onChange={e => setCharacter((c) => { return { ...c, name: pascalCase(e.target.value) } })}
              />
              <Form.Select
                fluid
                value={data?.element}
                defaultValue={data?.element}
                label='Element'
                loading={!data}
                disabled={!data}
                options={elementsOptions}
                placeholder={'Select element'}
              />
            </Form.Group>
            <Form.Group inline>
              <label>5 star</label>
              <Form.Radio
                label=''
                defaultChecked={data?.is5Star}
                checked={character.is5Star}
                onClick={() => setCharacter((c) => { return { ...c, is5Star: !c.is5Star } })}
              />
              <label>Published</label>
              <Form.Radio
                label=''
                defaultChecked={data?.published}
                checked={character.published}
                onClick={() => setCharacter((c) => { return { ...c, published: !c.published } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                label='Biography / Introduction'
                defaultValue={data?.bio}
                placeholder='You can write in Markdown here!'
                onChange={e => setCharacter((c) => { return { ...c, bio: e.target.value } })}
              />
              <Form.TextArea
                label='Notes'
                defaultValue={data?.notes}
                placeholder='You can write in Markdown here!'
                onChange={e => setCharacter((c) => { return { ...c, notes: e.target.value } })}
              />
            </Form.Group>
            <Grid columns={6}>
              <Grid.Column>
                <div {...getPortraitRootProps()}>
                  <Form.Button label='Portrait image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={filePortrait?.preview} size='small'></Image>
              </Grid.Column>
              <Grid.Column>
                <div {...getSplashRootProps()}>
                  <Form.Button label='Splash image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={fileSplash?.preview} size='medium'></Image>
              </Grid.Column>
            </Grid>
          </Form>
          <Header as='h2'>
            Advanced editor
          </Header>
          <Editor
            height="50vh"
            defaultLanguage="json"
            value={JSON.stringify(character, null, 4)}
            onChange={(e) => setCharacter(JSON.parse(e ?? ''))}
          />
          <Form>
            <Form.Button
              onClick={() => saveCharacter()}
              disabled={!JSON.stringify(character)}
            >
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
