import { Character, Element } from '@prisma/client'
import type { NextPage } from 'next'
import { useState } from 'react'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import { useSWRConfig } from 'swr'
import Layout from '../../../components/Layout'
import { useUpload } from '../../../hooks/useUpload'
import { allKeysSet } from '../../../utils/objects'
import { pascalCase } from '../../../utils/strings'

const elements = ["pyro", "hydro", "anemo", "electro", "dendro", "cryo", "geo"]
const elementsOptions = elements.map(e => {
  return {
    key: e,
    text: pascalCase(e),
    value: e.toUpperCase(),
  }
})

const Home: NextPage = () => {
  const [character, setCharacter] = useState<Omit<Character, 'id' | 'buildId' | 'createdAt' | 'teamCompId'>>({
    name: '',
    portrait: '',
    splash: '',
    notes: '',
    bio: '',
    element: 'ANEMO',
    is5Star: false,
    published: false
  })

  const { mutate } = useSWRConfig()

  const saveCharacter = () => fetch('/api/characters', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(character)
  })

  const [filePortrait, setPortraitFile] = useState<{ preview: string, name: string }>()
  const [fileSplash, setSplashFile] = useState<{ preview: string, name: string }>()

  const setSplashFileCallback = (file: any) => setSplashFile(file)
  const setSplashDataCallback = (data: string) => setCharacter(a => { return { ...a, splash: data } })

  const getSplashRootProps = useUpload(setSplashFileCallback, setSplashDataCallback)

  const setPortraitFileCallback = (file: any) => setPortraitFile(file)
  const setPortraitDataCallback = (data: string) => setCharacter(a => { return { ...a, portrait: data } })

  const getPortraitRootProps = useUpload(setPortraitFileCallback, setPortraitDataCallback)

  return (
    <Layout>
      <Header as='h2' dividing>
        Add character
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          <Form>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                label='Name'
                placeholder='Raiden Shogun'
                onChange={e => setCharacter((w) => { return { ...w, name: pascalCase(e.target.value) } })}
              />
              <Form.Select
                fluid
                label='Element'
                options={elementsOptions}
                placeholder={'Select element'}
                onChange={(_, { value }) => setCharacter((w) => { return { ...w, element: value as Element } })}
              />
            </Form.Group>
            <Form.Group inline>
              <label>5 star</label>
              <Form.Radio
                label=''
                checked={character.is5Star}
                onClick={() => setCharacter((w) => { return { ...w, is5Star: !w.is5Star } })}
              />
              <label>Published</label>
              <Form.Radio
                label=''
                checked={character.published}
                onClick={() => setCharacter((w) => { return { ...w, published: !w.published } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                label='Biography / Introduction'
                placeholder='You can write in Markdown here!'
                onChange={e => setCharacter((w) => { return { ...w, bio: e.target.value } })}
              />
              <Form.TextArea
                label='Notes'
                placeholder='You can write in Markdown here!'
                onChange={e => setCharacter((w) => { return { ...w, notes: e.target.value } })}
              />
            </Form.Group>

            <Grid columns={6}>
              <Grid.Column>
                <div {...getPortraitRootProps()}>
                  <Form.Button label='Portrait image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={filePortrait?.preview} size='small'></Image>
              </Grid.Column>
              <Grid.Column>
                <div {...getSplashRootProps()}>
                  <Form.Button label='Splash image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={fileSplash?.preview} size='medium'></Image>
              </Grid.Column>
            </Grid>

            <Form.Button
              style={{ marginTop: '2rem' }}
              onClick={() => saveCharacter().then(() => mutate('/api/characters'))}
              disabled={
                !(allKeysSet<Character>(character))
              }
            >
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
