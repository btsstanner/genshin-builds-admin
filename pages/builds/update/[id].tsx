import Editor from '@monaco-editor/react'
import { Artifact, Build, Character, Weapon } from '@prisma/client'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { omit } from 'radash'
import { useEffect, useState } from 'react'
import { Button, Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import useSWR, { Fetcher } from 'swr'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { optionify } from '../../../utils/objects'

const Home: NextPage = () => {
  const [uploading, setUploading] = useState(false)
  const [doneUploading, setDoneUploading] = useState(false)

  const router = useRouter()
  const { id } = router.query

  const [build, setBuild] = useState<Partial<Build>>({})

  const fetcher: Fetcher<Build, string> = (...args) => fetch(...args).then(res => res.json())
  const { data } = useSWR(`/api/builds/${id}`, fetcher)

  useEffect(() => setBuild(data ?? {}), [data])

  const addBuild = () => {
    const body: Partial<Build> = omit(build, ['id'])
    fetch(`/api/builds/${id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
      .then(() => setUploading(true))
      .then(() => setUploading(false))
      .then(() => setDoneUploading(true))
  }

  return (
    <Layout>
      <Header as='h2' dividing>
        Edit build
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploading ? <Loader title='Uploading' text="Uploading artifact's data"></Loader> : null}
          {doneUploading ? <Loader dismissable success title='Success' text="Uploaded artifact's data"></Loader> : null}
          <Editor
            height="65vh"
            defaultLanguage="json"
            value={JSON.stringify(build, null, 2)}
          />
          <Form>
            <Form.Button
              onClick={addBuild}
              style={{ marginTop: '2rem' }}
            >
              <Icon name='save'></Icon>
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
