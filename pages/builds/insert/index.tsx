import { Artifact, Build, Character, Weapon } from '@prisma/client'
import type { NextPage } from 'next'
import { useState } from 'react'
import { Button, Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import useSWR, { Fetcher } from 'swr'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { optionify } from '../../../utils/objects'

const Home: NextPage = () => {
  const [uploading, setUploading] = useState(false)
  const [doneUploading, setDoneUploading] = useState(false)

  const [selectedCharacter, setSelectedCharacter] = useState<Character>()

  const [selectedWeapon, setSelectedWeapon] = useState<Weapon>()

  const [selectedArtifacts4, setSelectedArtifacts4] = useState<Artifact>()
  const [selectedArtifacts2, setSelectedArtifacts2] = useState<Artifact[]>(new Array().fill(2))

  const [build, setBuild] = useState<Partial<Build>>({})

  const fetcherWeapons: Fetcher<Weapon[], string> = (...args) => fetch(...args).then(res => res.json())
  const fetcherArtifacts: Fetcher<Artifact[], string> = (...args) => fetch(...args).then(res => res.json())
  const fetcherCharacters: Fetcher<Character[], string> = (...args) => fetch(...args).then(res => res.json())

  const weapons = useSWR('/api/weapons', fetcherWeapons).data ?? []
  const artifacts = useSWR('/api/artifacts', fetcherArtifacts).data ?? []
  const characters = useSWR('/api/characters', fetcherCharacters).data ?? []

  const addBuild = () => {
    const body: Partial<Build> = {
      ...build,
      artifactsPairs: selectedArtifacts2.map(a => a.id),
      artifactsQuads: [selectedArtifacts4?.id ?? '']
    }
    fetch('/api/builds', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
      .then(() => setUploading(true))
      .then(() => setUploading(false))
      .then(() => setDoneUploading(true))
  }

  return (
    <Layout>
      <Header as='h2' dividing>
        Add build for character
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploading ? <Loader title='Uploading' text="Uploading artifact's data"></Loader> : null}
          {doneUploading ? <Loader dismissable success title='Success' text="Uploaded artifact's data"></Loader> : null}
          <Form>
            <Form.Group widths='equal'>
              <Form.Input
                label='Role'
                placeholder='DPS/Support/Burst support...'
                onChange={(e) => setBuild(b => { return { ...b, role: e.target.value } })}
              />
              <Form.Select
                search
                required
                loading={characters.length === 0}
                placeholder='Select character'
                label='Character'
                options={characters.map(optionify)}
                onChange={(_, { value }) => {
                  setSelectedCharacter(characters.find(f => f.id === value))
                  setBuild(b => { return { ...b, characterId: value?.toString() } })
                }}
              />
              <Form.Select
                search
                required
                loading={weapons.length === 0}
                placeholder='Select weapon'
                label='Weapon'
                options={weapons.map(optionify)}
                onChange={(_, { value }) => {
                  setSelectedWeapon(weapons.find(f => f.id === value))
                  setBuild(b => { return { ...b, weaponId: value?.toString() } })
                }}
              />
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Select
                search
                loading={artifacts.length === 0}
                placeholder='Select artifacts set (4)'
                label='Artifacts set (full)'
                options={artifacts.map(optionify)}
                onChange={(_, { value }) => {
                  setSelectedArtifacts4(artifacts.find(f => f.id === value))
                }}
              />
              <Form.Select
                search
                loading={artifacts.length === 0}
                placeholder='Select artifacts set (2)'
                label='Artifacts set (first pair)'
                options={artifacts.map(optionify)}
                onChange={(_, { value }) => {
                  setSelectedArtifacts2(pair => {
                    const updated = [...pair]
                    updated[0] = artifacts.find(f => f.id === value)!
                    return updated
                  })
                }}
              />
              <Form.Select
                search
                loading={artifacts.length === 0}
                placeholder='Select artifacts set (2)'
                label='Artifacts set (second pair)'
                options={artifacts.map(optionify)}
                onChange={(_, { value }) => {
                  setSelectedArtifacts2(pair => {
                    const updated = [...pair]
                    updated[1] = artifacts.find(f => f.id === value)!
                    return updated
                  })
                }}
              />
            </Form.Group>

            <Header as='h2'>
              Live build
            </Header>

            <Grid columns={6}>
              <Grid.Column>
                <Image
                  label={selectedCharacter?.name ?? 'Character'}
                  fluid
                  rounded
                  style={{ backgroundColor: selectedCharacter?.is5Star ? 'orange' : 'transparent' }}
                  src={selectedCharacter?.portrait}
                />
                {selectedCharacter ?
                  <Button style={{ marginTop: '1.25rem' }} onClick={() => setSelectedCharacter(undefined)}>
                    Clear
                  </Button> : null
                }
              </Grid.Column>
              <Grid.Column>
                <Image
                  label={selectedWeapon?.name ?? 'Weapon'}
                  fluid
                  rounded
                  src={selectedWeapon?.img}
                />
                {selectedWeapon ?
                  <Button style={{ marginTop: '1.25rem' }} onClick={() => setSelectedWeapon(undefined)}>
                    Clear
                  </Button> : null
                }
              </Grid.Column>
              <Grid.Column>
                <Image
                  label={`Full ${selectedArtifacts4?.name ?? '-'}`}
                  fluid
                  rounded
                  src={selectedArtifacts4?.img}
                />
                {selectedArtifacts4 ?
                  <Button style={{ marginTop: '1.25rem' }} onClick={() => setSelectedArtifacts4(undefined)}>
                    Clear
                  </Button> : null
                }

              </Grid.Column>
              <Grid.Column>
                <Image
                  label={`Half ${selectedArtifacts2[0]?.name ?? '-'}`}
                  fluid
                  rounded
                  src={selectedArtifacts2[0]?.img}
                />
                {selectedArtifacts2.at(0) || selectedArtifacts2.at(1) ?
                  <Button style={{ marginTop: '1.25rem' }} onClick={() => setSelectedArtifacts2(new Array().fill(2))}>
                    Clear
                  </Button> : null
                }
              </Grid.Column>
              <Grid.Column>
                <Image
                  label={`Half ${selectedArtifacts2[1]?.name ?? '-'}`}
                  fluid
                  rounded
                  src={selectedArtifacts2[1]?.img}
                />
              </Grid.Column>
            </Grid>

            <Header as={'h2'}>
              Useful data
            </Header>

            <Form.Group widths='equal'>
              <Form.Input
                placeholder='Comma separated [sands, goblet, circlet]'
                label='Artifacts main stats'
                onChange={(e) => setBuild(b => {
                  return {
                    ...b,
                    artifactsMainStats: e.target.value.split(',').map(w => w.trim())
                  }
                })}
              />
              <Form.Input
                placeholder='Comma separated [any, any]'
                label='Artifacts sub stats'
                onChange={(e) => setBuild(b => {
                  return {
                    ...b,
                    artifactsSubStats: e.target.value.split(',').map(w => w.trim())
                  }
                })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                placeholder='Markdown is allowed'
                label='Ability tips'
                onChange={(e) => setBuild(b => {
                  return {
                    ...b,
                    abilityTips: e.target.value
                  }
                })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Input
                placeholder='Comma separated'
                label='Talents priority'
                onChange={(e) => setBuild(b => {
                  return {
                    ...b,
                    talentsPriority: e.target.value.split(',').map(w => w.trim())
                  }
                })}
              />
              <Form.Select
                placeholder='Optimal build'
                label='Optimal'
                options={
                  [
                    { name: 'No', id: false },
                    { name: 'Yes', id: true },
                  ].map(optionify)
                }
                onChange={(_, { value }) => {
                  setBuild(b => {
                    return {
                      ...b,
                      optimal: Boolean(value)
                    }
                  })
                }}
              />
            </Form.Group>

            <Form.Button
              onClick={addBuild}
              style={{ marginTop: '2rem' }}
            >
              <Icon name='save'></Icon>
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
