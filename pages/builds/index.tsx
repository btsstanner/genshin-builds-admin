import { Build, Character, Weapon } from '@prisma/client'
import type { NextPage } from 'next'
import Link from 'next/link'
import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { Button, Grid, Header, Icon, Image, Input, Modal, Table } from 'semantic-ui-react'
import useSWR, { Fetcher, useSWRConfig } from 'swr'
import Layout from '../../components/Layout'
import Loader from '../../components/Loader'
import { pascalCase } from '../../utils/strings'

type BuildLeftJoinCharacter = Build & { character: Character, equipment: Weapon }

const Home: NextPage = () => {
  const [openModal, setOpenModal] = useState(false)
  const [target, setTarget] = useState<Build>()

  const fetcher: Fetcher<BuildLeftJoinCharacter[], string> = (...args) => fetch(...args).then(res => res.json())
  const { data } = useSWR('/api/builds', fetcher)

  const [workingData, setWorkingData] = useState<BuildLeftJoinCharacter[]>(new Array<BuildLeftJoinCharacter>())

  useEffect(() => {
    setWorkingData(data ?? [])
  }, [data])

  const search = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setWorkingData(
      (data ?? []).filter(el =>
        el.character.name.toLowerCase().includes(e.target.value.toLowerCase()))
    )
  }, [data])

  const { mutate } = useSWRConfig()

  const deleteBuild = (id: string) => fetch(`/api/builds/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(() => mutate('/api/builds'))

  return (
    <Layout>
      <Header as='h2' dividing>
        Characters builds
      </Header>

      <Grid columns='equal'>
        <Grid.Column >
          <Link href='/builds/insert'>
            <Button>
              Add new build
            </Button>
          </Link>
        </Grid.Column>
        <Grid.Column width={13}>
          <Input icon='search' placeholder='Search...' onChange={search} fluid />
        </Grid.Column>
      </Grid>

      <Grid columns={1} style={{ marginTop: '0.25rem' }}>
        <Grid.Column>
          {!data && workingData.length === 0 ? <Loader title='Loading' text="Loading weapons' data..." /> : null}
          <Table basic='very' celled >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Character</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {workingData?.map(w => (
                <Table.Row key={w.id}>
                  <Table.Cell>
                    <Header as='h4' image>
                      <Image src={w.character.portrait ?? ''} />
                      <Header.Content>
                        {w.character.name}
                        <Header.Subheader>
                          Role: {w.role}
                        </Header.Subheader>
                        <Header.Subheader>
                          Weapon: {w.equipment?.name}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                  </Table.Cell>
                  <Table.Cell>
                    <Button.Group>
                      <Link href={`/builds/update/${w.id}`}>
                        <Button>Edit</Button>
                      </Link>
                      <Button.Or text='or' />
                      <Button color='red' onClick={() => {
                        setOpenModal(true)
                        setTarget(w)
                      }}>
                        Delete
                      </Button>
                    </Button.Group>
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid>
      <Modal
        basic
        open={openModal}
        size='small'
      >
        <Header icon>
          <Icon name='archive' />
          {`Delete ${target?.id}???`}
        </Header>
        <Modal.Content>
          <p>
            This operation is irreversible.
          </p>
          <p>Are you really sure to delete it?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button basic color='red' inverted onClick={() => {
            setOpenModal(false)
          }}>
            <Icon name='remove' /> No
          </Button>
          <Button color='green' inverted onClick={() => {
            deleteBuild(target?.id ?? '')
            setOpenModal(false)
          }}>
            <Icon name='checkmark' /> Yes
          </Button>
        </Modal.Actions>
      </Modal>
    </Layout>
  )
}

export default Home
