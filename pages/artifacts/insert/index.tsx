import { Artifact } from '@prisma/client'
import type { NextPage } from 'next'
import { useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { pascalCase } from '../../../utils/strings'

const emptyArtifact = {
  name: '',
  img: '',
  twoPiecesEffect: '',
  fourPiecesEffect: '',
}

const Home: NextPage = () => {
  const [artifact, setArtifact] = useState<Omit<Artifact, 'id' | 'buildId' | 'createdAt'>>(emptyArtifact)

  const [uploading, setUploading] = useState(false)
  const [doneUploading, setDoneUploading] = useState(false)

  const addArtifact = () => fetch('/api/artifacts', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(artifact)
  })
    .then(() => setUploading(true))
    .then(() => setUploading(false))
    .then(() => setDoneUploading(true))

  const [file, setFile] = useState<{ preview: string, name: string }>()

  const { getRootProps, } = useDropzone({
    accept: { 'image/*': [] },
    onDrop: acceptedFiles => {
      const file = Object.assign(acceptedFiles[0], {
        preview: URL.createObjectURL(acceptedFiles[0])
      })
      const reader = new FileReader()
      reader.readAsDataURL(file);
      reader.onload = (() => {
        setFile(file)
        setArtifact(a => { return { ...a, img: reader.result as string } })
      })
    }
  });

  return (
    <Layout>
      <Header as='h2' dividing>
        Add artifacts set
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploading ? <Loader title='Uploading' text="Uploading artifact's data"></Loader> : null}
          {doneUploading ? <Loader dismissable success title='Success' text="Uploaded artifact's data"></Loader> : null}
          <Form>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                label='Set name'
                placeholder='Wanderers Troupe'
                onChange={e => setArtifact((a) => { return { ...a, name: pascalCase(e.target.value) } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                label='Two pieces effect'
                placeholder='Two pieces effect, +18ATK%... 20%ER Bonus...'
                onChange={e => setArtifact((a) => { return { ...a, twoPiecesEffect: e.target.value } })}
              />
              <Form.TextArea
                label='Four pieces effect'
                placeholder='Four pieces effect description'
                onChange={e => setArtifact((a) => { return { ...a, fourPiecesEffect: e.target.value } })}
              />
            </Form.Group>
            {/* <Form.Input
              fluid
              label='Image URL'
              placeholder='https://...'
              error={!isURL(artifact.img ?? '')}
              onChange={e => setArtifact((a) => { return { ...a, img: e.target.value } })}
            /> */}
            <Grid columns={6}>
              <Grid.Column>
                <div {...getRootProps()}>
                  <Form.Button label='Image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
              <Grid.Column floated='left'>
                <Image src={file?.preview} size='tiny'></Image>
              </Grid.Column>
            </Grid>

            <Form.Button
              style={{ marginTop: '2rem' }}
              onClick={() => addArtifact()}
              disabled={!(artifact.name && artifact.img && artifact.twoPiecesEffect && artifact.fourPiecesEffect)}
            >
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
