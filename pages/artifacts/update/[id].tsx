import { Artifact } from '@prisma/client'
import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { omit } from 'radash'
import { Fragment, useEffect, useState } from 'react'
import { Form, Grid, Header, Icon, Image } from 'semantic-ui-react'
import useSWR, { Fetcher, useSWRConfig } from 'swr'
import Layout from '../../../components/Layout'
import Loader from '../../../components/Loader'
import { useUpload } from '../../../hooks/useUpload'
import { pascalCase } from '../../../utils/strings'

const Home: NextPage = () => {
  const [artifact, setArtifact] = useState<Omit<Artifact, 'buildId' | 'createdAt'>>({
    id: '',
    name: '',
    img: '',
    twoPiecesEffect: '',
    fourPiecesEffect: '',
  })

  const [uploading, setUploading] = useState(false)
  const [doneUploading, setDoneUploading] = useState(false)

  const fetcher: Fetcher<Artifact, string> = (...args) => fetch(...args).then(res => res.json())
  const router = useRouter()
  const { id } = router.query

  const { data } = useSWR(`/api/artifacts/${id}`, fetcher)
  const { mutate } = useSWRConfig()

  useEffect(() => {
    if (data) {
      setArtifact(data)
      setOldFile(data.img ?? '')
    }
  }, [data])

  const editArtifact = () => {
    setUploading(true)
    fetch(`/api/artifacts/${data?.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(omit(artifact, ['id']))
    })
      .then(() => mutate('/api/artifacts'))
      .then(() => setUploading(false))
      .then(() => setDoneUploading(true))
  }

  const [file, setFile] = useState<{ preview: string, name: string }>()
  const [oldFile, setOldFile] = useState("")

  const setFileCallback = (file: any) => setFile(file)
  const setDataCallback = (data: string) => setArtifact(a => { return { ...a, img: data } })

  const getRootProps = useUpload(setFileCallback, setDataCallback)

  return (
    <Layout>
      <Header as='h2' dividing>
        Edit artifact
      </Header>

      <Grid columns={1} stackable>
        <Grid.Column>
          {uploading ? <Loader title='Uploading' text="Uploading artifact's data"></Loader> : null}
          {doneUploading ? <Loader dismissable success title='Success' text="Uploaded artifact's data"></Loader> : null}
          <Form loading={!data || uploading}>
            <Form.Group widths='equal'>
              <Form.Input
                fluid
                defaultValue={data?.name}
                label='Set name'
                placeholder='Wanderers Troupe'
                loading={!data}
                onChange={e => setArtifact((a) => { return { ...a, name: pascalCase(e.target.value) } })}
              />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea
                defaultValue={data?.twoPiecesEffect}
                label='Two pieces effect'
                placeholder='Two pieces effect, +18ATK%... 20%ER Bonus...'
                onChange={e => setArtifact((a) => { return { ...a, twoPiecesEffect: e.target.value } })}
              />
              <Form.TextArea
                defaultValue={data?.fourPiecesEffect}
                label='Four pieces effect'
                placeholder='Four pieces effect description'
                onChange={e => setArtifact((a) => { return { ...a, fourPiecesEffect: e.target.value } })}
              />
            </Form.Group>
            <Grid columns={5}>
              <Grid.Column>
                <label>Image</label>
                <Image src={oldFile} fluid></Image>
              </Grid.Column>
              <Grid.Column>
                {
                  file?.preview ?
                    <Fragment>
                      <label>Will be replaced with</label>
                      <Image src={artifact.img ?? data?.img} fluid></Image>
                    </Fragment> :
                    null
                }
              </Grid.Column>
            </Grid>
            <Grid columns={6}>
              <Grid.Column>
                <div {...getRootProps()}>
                  <Form.Button label='Image'>
                    <Icon name='upload'></Icon>
                    Click to upload
                  </Form.Button>
                </div>
              </Grid.Column>
            </Grid>
            <Form.Button
              style={{ marginTop: '2rem' }}
              onClick={() => editArtifact()}
              disabled={!(artifact.name && artifact.img && artifact.twoPiecesEffect && artifact.fourPiecesEffect)}
            >
              Save
            </Form.Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
