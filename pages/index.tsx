import type { NextPage } from 'next'
import { Grid, Header } from 'semantic-ui-react'
import Layout from '../components/Layout'
import useUser from '../hooks/useUser'

const Home: NextPage = () => {
  const { data } = useUser({
    redirectTo: '/login',
  })

  return (
    <Layout>
      <Header as='h2' dividing>
        Home
      </Header>
      <Grid columns={1} stackable>
        <Grid.Column>
          <Header as={'h3'} style={{ paddingTop: '2rem' }}>
            {`${data?.login}, welcome to GenshinBuildsAdmin ✨`}
          </Header>
          <p>Here you can manage artifacts, weapons, characters and their builds.</p>
          <div>Everything you need is in the navbars&apos; menu.</div>
        </Grid.Column>
      </Grid>
    </Layout>
  )
}

export default Home
