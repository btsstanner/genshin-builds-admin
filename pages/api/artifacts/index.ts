import { Artifact } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'
import s3 from '../../../lib/s3'
import { sessionOptions } from '../../../lib/session'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Artifact | Artifact[] | string>
) {
  if (req.session.user) {
    if (req.method === 'POST') {
      try {
        let created = await prisma.artifact.create({
          data: { ...req.body }
        })
        if (req.body.img) {
          const s3res = await s3.putObject(created.id.toString()!, req.body.img)
          created = await prisma.artifact.update({
            where: { id: created.id },
            data: { img: s3res.url }
          })
        }
        return res.status(200).json(created)
      } catch (e) {
        return res.status(500)
      }
    }

    if (req.method === 'GET') {
      const artifacts = await prisma.artifact.findMany({
        orderBy: {
          name: 'asc'
        }
      })
      return res.status(200).json(artifacts)
    }

    return res.status(405).send(`Method ${req.method} not allowed`)
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)