import { Artifact } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../../lib/primsa'
import { sessionOptions } from '../../../../lib/session'
import { pascalCase } from '../../../../utils/strings'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Artifact | null | string>
) {
  let { name } = req.query
  name = pascalCase(name?.toString()!)

  if (req.session.user) {
    const artifacts = await prisma.artifact.findFirst({
      where: {
        OR: [
          {
            name: {
              equals: name,
              mode: 'insensitive'
            }
          },
          {
            name: {
              equals: `The ${name}`,
              mode: 'insensitive'
            }
          },
          {
            name: {
              equals: `"${name}"`,
              mode: 'insensitive'
            }
          },
          {
            name: {
              equals: `${name.replace("-", " ")}`,
              mode: 'insensitive'
            }
          },
          {
            name: {
              equals: `${name.replace("s", "'s")}`,
              mode: 'insensitive'
            }
          },
          {
            name: {
              equals: `${name.replace("s", "s'")}`,
              mode: 'insensitive'
            }
          },
        ]
      }
    })
    return res.status(200).json(artifacts)
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)
