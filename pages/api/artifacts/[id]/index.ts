import { Artifact } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import { shake } from 'radash'
import prisma from '../../../../lib/primsa'
import s3 from '../../../../lib/s3'
import { sessionOptions } from '../../../../lib/session'
import { isBase64Url } from '../../../../utils/strings'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Artifact | string | any>
) {
  const { id } = req.query

  if (req.method === 'GET' && req.session.user) {
    try {
      const artifact = await prisma.artifact.findUnique({
        where: {
          id: id?.toString()
        }
      })
      if (artifact) {
        return res.status(200).json(artifact)
        return
      }
      return res.status(404)
    } catch (e) {
      return res.status(500).send(e)
    }
    return
  }
  if (req.method === 'PATCH' && req.session.user) {
    try {
      let edited = await prisma.artifact.update({
        where: {
          id: id?.toString()
        },
        data: shake({ ...req.body, createdAt: new Date() })
      })
      if (req.body.img && isBase64Url(req.body.img)) {
        const s3res = await s3.putObject(edited.id.toString()!, req.body.img)
        edited = await prisma.artifact.update({
          where: { id: edited.id },
          data: { img: s3res.url }
        })
      }
      return res.status(200).json(edited)
    } catch (e) {
      return res.status(500).send(e)
    }
    return
  }
  if (req.method === 'DELETE' && req.session.user) {
    try {
      const deleted = await prisma.artifact.delete({
        where: {
          id: id?.toString()
        },
      })
      return res.status(200).json(deleted)
    } catch (e) {
      return res.status(500).send(e)
    }
  }
  return res.status(405).send(`Method ${req.method} not allowed`)
}

export default withIronSessionApiRoute(handler, sessionOptions)