import type { User } from './user'

import { withIronSessionApiRoute } from 'iron-session/next'
import { sessionOptions } from '../../lib/session'
import { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../lib/primsa'
import * as bcrypt from 'bcrypt'

async function loginRoute(req: NextApiRequest, res: NextApiResponse) {
  const { username, password } = await req.body

  try {
    const data = await prisma.user.findFirst({
      where: {
        username: username,
      }
    })

    const match = await bcrypt.compare(password, data?.password ?? '')

    if (!data) throw Error('not found')
    if (!match) throw Error('wrong password')

    const user = { isLoggedIn: true, login: data?.username } as User
    req.session.user = user
    await req.session.save()
    res.json(user)
  } catch (error) {
    res.status(500).json({ message: (error as Error).message })
  }
}

export default withIronSessionApiRoute(loginRoute, sessionOptions)
