import { Weapon } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'
import s3 from '../../../lib/s3'
import { sessionOptions } from '../../../lib/session'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Weapon | Weapon[] | string>
) {
  const { method } = req

  if (req.session.user) {
    if (method === 'GET') {
      const weapons = await prisma.weapon.findMany({
        orderBy: {
          name: 'asc'
        }
      })
      res.status(200).json(weapons)
      return
    }
    if (method === 'POST') {
      let created = await prisma.weapon.create({
        data: { ...req.body }
      })
      if (req.body.img) {
        const s3res = await s3.putObject(created.id, req.body.img)
        created = await prisma.weapon.update({
          where: { id: created.id },
          data: { img: s3res.url }
        })
      }
      res.status(200).json(created)
      return
    }
    return res.status(405)
  }
}

export default withIronSessionApiRoute(handler, sessionOptions)