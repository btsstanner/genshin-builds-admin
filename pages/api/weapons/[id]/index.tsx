import { Weapon } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import { shake } from 'radash'
import prisma from '../../../../lib/primsa'
import s3 from '../../../../lib/s3'
import { sessionOptions } from '../../../../lib/session'
import { isBase64Url } from '../../../../utils/strings'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Weapon | string>
) {
  const { id } = req.query

  if (req.session.user) {
    switch (req.method) {
      case 'GET':
        const weapon = await prisma.weapon.findUnique({
          where: { id: id?.toString() }
        })
        if (weapon) {
          res.status(200).json(weapon)
          return
        }
        res.status(404)
        return
      case 'PATCH':
        let edited = await prisma.weapon.update({
          where: {
            id: id?.toString()
          },
          data: shake({ ...req.body })
        })
        if (req.body.img && isBase64Url(req.body.img)) {
          const s3res = await s3.putObject(edited.id, req.body.img)
          edited = await prisma.weapon.update({
            where: { id: edited.id },
            data: { img: s3res.url }
          })
        }
        res.status(200).json(edited)
        return
      case 'DELETE':
        const deleted = await prisma.weapon.delete({
          where: { id: id?.toString() }
        })
        res.status(200).json(deleted)
        return
      default:
        res.status(405).send(`Method ${req.method} not allowed`)
    }
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)