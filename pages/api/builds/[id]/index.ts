import { Build } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import { shake } from 'radash'
import prisma from '../../../../lib/primsa'
import { sessionOptions } from '../../../../lib/session'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Build[] | Build | string>
) {
  const { method } = req
  const { id } = req.query

  if (req.session.user) {
    if (method === 'GET') {
      const build = await prisma.build.findUnique({
        where: { id: id?.toString() }
      })
      return build ?
        res.status(200).json(build) :
        res.status(404).send('not found')
    }
    if (method === 'PATCH') {
      const edited = await prisma.build.update({
        data: shake({ ...req.body }),
        where: { id: id?.toString() }
      })
      return res.status(200).json(edited)
    }
    if (method === 'DELETE') {
      const deleted = await prisma.build.delete({
        where: { id: id?.toString() }
      })
      return res.status(200).json(deleted)
    }
    return res.status(405).send('Method not allowed')
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)