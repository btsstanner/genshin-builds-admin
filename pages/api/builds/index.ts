import { Build } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'
import { sessionOptions } from '../../../lib/session'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Build[] | Build | string>
) {
  const { method } = req

  if (req.session.user) {
    if (method === 'GET') {
      let builds: any[] = await prisma.build.findMany({
        where: {
          isUserGenerated: {
            not: true
          }
        },
        include: { equipment: true }
      })
      for await (const build of builds) {
        build.character = await prisma.character.findUnique({
          where: { id: build.characterId },
          select: { name: true, element: true, portrait: true }
        })
      }
      return res.status(200).json(builds)
    }
    if (method === 'POST') {
      const created = await prisma.build.create({
        data: { ...req.body, isUserGenerated: false }
      })
      return res.status(200).json(created)
    }
    return res.status(405).send('Method not allowed')
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)