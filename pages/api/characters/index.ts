import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/primsa'
import s3 from '../../../lib/s3'
import { sessionOptions } from '../../../lib/session'
import { isBase64Url } from '../../../utils/strings'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any[] | any>
) {
  if (req.method === 'POST' && req.session.user) {
    try {
      let created = await prisma.character.create({
        data: { ...req.body }
      })
      if (req.body.portrait && isBase64Url(req.body.portrait)) {
        const s3res = await s3.putObject(`${created.id}-portrait`, req.body.portrait)
        created = await prisma.character.update({
          where: { id: created.id },
          data: {
            portrait: s3res.url,
          }
        })
      }
      if (req.body.splash && isBase64Url(req.body.splash)) {
        const s3res = await s3.putObject(`${created.id}-splash`, req.body.splash)
        created = await prisma.character.update({
          where: { id: created.id },
          data: {
            splash: s3res.url,
          }
        })
      }
      return res.status(200).json(created)
    } catch (e) {
      return res.status(500).send(e)
    }
  }

  if (req.method === 'GET') {
    const characters = await prisma.character.findMany({
      distinct: ['name'],
      orderBy: {
        name: 'asc'
      }
    })

    return res.status(200).json(characters)
  }

  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)