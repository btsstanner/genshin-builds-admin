import { Character, Element } from '@prisma/client'
import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../../lib/primsa'
import { sessionOptions } from '../../../../lib/session'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Character[] | string>
) {
  const element = req.query.element?.toString().toUpperCase() as Element

  if (req.session.user) {
    if (req.session.user && req.method === 'GET') {
      const characters = await prisma.character.findMany({
        where: {
          element: {
            equals: element
          }
        }
      })
      res.status(200).json(characters)
    }
    return res.status(401).send('not authorized')
  }
  return res.status(401).send('not authorized')
}

export default withIronSessionApiRoute(handler, sessionOptions)