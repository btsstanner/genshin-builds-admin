import { withIronSessionApiRoute } from 'iron-session/next'
import type { NextApiRequest, NextApiResponse } from 'next'
import { map, omit } from 'radash'
import prisma from '../../../../lib/primsa'
import s3 from '../../../../lib/s3'
import { sessionOptions } from '../../../../lib/session'
import { isBase64Url } from '../../../../utils/strings'

async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const { id, extended } = req.query

  if (req.session.user) {
    if (req.method === 'GET') {
      try {
        let character: any = await prisma.character.findUnique({
          where: { id: id?.toString() }
        })

        const builds = await prisma.build.findMany({
          where: { characterId: id?.toString() },
          include: { equipment: true }
        })

        character.builds = builds

        if (extended === 'true' && character) {
          let quads: any[] = []
          let pairs: any[] = []
          for await (const build of character.builds) {
            quads = await map(build.artifactsQuads, async (id: string) => {
              return await prisma.artifact.findUnique({ where: { id } })
            })
            pairs = await map(build.artifactsPairs, async (id: string) => {
              return await prisma.artifact.findUnique({ where: { id } })
            })
          }

          character.builds = character.builds.map((b: any) => {
            return {
              ...b, artifactsQuads: quads, artifactsPairs: pairs
            }
          })
        }

        return res.status(200).json(character)
      } catch (e) {
        return res.status(500).send(e)
      }
    }
    if (req.method === 'PATCH') {
      try {
        let edited = await prisma.character.update({
          where: {
            id: id?.toString()
          },
          data: {
            ...omit({ ...req.body }, ['builds']),
          }
        })
        if (req.body.portrait && !String(req.body.splash).startsWith('http')) {
          const s3res = await s3.putObject(`${edited.id}-portrait`, req.body.portrait)
          edited = await prisma.character.update({
            where: { id: edited.id },
            data: { portrait: s3res.url }
          })
        }
        if (req.body.splash && !String(req.body.splash).startsWith('http')) {
          const s3res = await s3.putObject(`${edited.id}-splash`, req.body.splash)
          edited = await prisma.character.update({
            where: { id: edited.id },
            data: { splash: s3res.url }
          })
        }
        return res.status(200).json(edited)
      } catch (e) {
        return res.status(500).send(e)
      }
    }
    return res.status(405).send(`Method ${req.method} not allowed`)
  }
  return res.status(401).send('not authorized')
}


export default withIronSessionApiRoute(handler, sessionOptions)