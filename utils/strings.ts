export const pascalCase = (target: string) => {
  const skipWords = ["of", "an", "a"]

  return target
    .toLowerCase()
    .split(" ")
    .map(word => {
      if (skipWords.includes(word.toLowerCase())) {
        return word.toLowerCase()
      }
      return `${word.charAt(0).toUpperCase()}${word.substring(1)}`
    })
    .reduce((prev, curr) => `${prev} ${curr}`)
    .trimEnd()
}

export const isURL = (target: string) => {
  return !!target.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/gm)
}

export const isBase64Url = (target: string) => {
  return target.startsWith("data:image/webp;base64")
}