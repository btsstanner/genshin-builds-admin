import { shake } from "radash"

export function allKeysSet<T>(target: any) {
  type Control = { [P in keyof Required<T>]: true }

  const control: Control = target
  const shaked = shake(target)

  return Object.keys(control).length === Object.keys(shaked).length
}

export const optionify = (e: any) => {
  return {
    key: e.id,
    text: e.name,
    value: e.id
  }
}