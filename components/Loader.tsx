import { Fragment, useState } from "react"
import { Icon, Message } from "semantic-ui-react"

const Loader = ({ title, text, dismissable = false, success }: { title: string, text: string, dismissable?: boolean, success?: boolean }) => {
  const [show, setShow] = useState(true)

  if (dismissable && show) {
    return (
      <Message icon onDismiss={() => setShow(false)}>
        <Icon name={success ? 'check' : 'circle notched'} loading={!success} />
        <Message.Content>
          <Message.Header>{title}</Message.Header>
          {text}
        </Message.Content>
      </Message>
    )
  }
  if (show) {
    return (
      <Message icon>
        <Icon name={success ? 'check' : 'circle notched'} loading />
        <Message.Content>
          <Message.Header>{title}</Message.Header>
          {text}
        </Message.Content>
      </Message>
    )
  }
  return <Fragment />
}

export default Loader