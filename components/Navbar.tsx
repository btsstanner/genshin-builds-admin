import { FC } from "react"
import { Container, Dropdown, Image, Menu } from "semantic-ui-react"

const Navbar: FC = () => {
  return (
    <Menu fixed='top' inverted>
      <Container>
        <Menu.Item as='a' header>
          <Image size='mini' src='/primogem.webp' style={{ marginRight: '1.5em' }} />
          Pepper&Miko Admin
        </Menu.Item>
        <Menu.Item as='a' href='/'>
          Home
        </Menu.Item>
        <Dropdown item simple text='Menu'>
          <Dropdown.Menu>
            <Dropdown.Item as='a' href='/artifacts'>
              Artifacts
            </Dropdown.Item>
            <Dropdown.Item as='a' href='/weapons'>
              Weapons
            </Dropdown.Item>
            <Dropdown.Item as='a' href='/characters'>
              Characters
            </Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Header>
              Misc
            </Dropdown.Header>
            <Dropdown.Item>
              <i className='dropdown icon' />
              <span className='text'>Submenu</span>
              <Dropdown.Menu>
                <Dropdown.Item as='a' href='/builds'>
                  Builds
                </Dropdown.Item>
                <Dropdown.Item>Resources</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Container>
    </Menu>
  )
}

export default Navbar