import Head from "next/head";
import { FC, Fragment } from "react";
import { Container } from "semantic-ui-react";
import useUser from "../hooks/useUser";
import Navbar from "./Navbar";

type Props = {
  children: JSX.Element | JSX.Element[]
}

const Layout: FC<Props> = ({ children }) => {
  useUser({
    redirectTo: '/login',
  })
  return (
    <Fragment>
      <Head>
        <title>GenshinJikken Admin</title>
      </Head>
      <Navbar></Navbar>
      <Container style={{ marginTop: '6.75rem' }}>
        {children}
      </Container>
    </Fragment>
  )
}

export default Layout