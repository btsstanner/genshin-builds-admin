import { useDropzone } from "react-dropzone";

export const useUpload = (onFile: Function, onLoad: Function) => {
  const { getRootProps } = useDropzone({
    accept: { 'image/*': [] },
    onDrop: acceptedFiles => {
      const file = Object.assign(acceptedFiles[0], {
        preview: URL.createObjectURL(acceptedFiles[0])
      })
      const reader = new FileReader()
      reader.readAsDataURL(file);
      reader.onload = (() => {
        onFile(file)
        onLoad(reader.result as String)
      })
    }
  })
  return getRootProps
}
