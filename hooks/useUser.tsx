import { useEffect } from 'react'
import Router from 'next/router'
import useSWR, { Fetcher } from 'swr'
import { User } from '../pages/api/user'

export default function useUser({
  redirectTo = '',
} = {}) {
  const fetcher: Fetcher<User, string> = (...args) => fetch(...args).then(res => res.json())
  const { data, mutate } = useSWR('/api/user', fetcher)

  useEffect(() => {
    if (!data) {
      return
    }
    if (!data.isLoggedIn) {
      Router.push('/login')
    }
  }, [data, redirectTo])

  return { data, mutate }
}
